package com.maximemichel.presentation.ui.base

import androidx.appcompat.app.AppCompatActivity

abstract class BaseActivity : AppCompatActivity() {

    fun getCurrentFragment(): BaseFragment? {
        return supportFragmentManager.primaryNavigationFragment
            ?.childFragmentManager?.primaryNavigationFragment as? BaseFragment
    }
}
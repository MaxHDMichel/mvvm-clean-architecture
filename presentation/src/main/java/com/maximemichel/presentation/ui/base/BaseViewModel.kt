package com.maximemichel.presentation.ui.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.maximemichel.domain.error.AppError

open class BaseViewModel : ViewModel() {

    @Suppress("VariableNaming")
    protected val _appError = MutableLiveData<AppError?>()
    val appError: LiveData<AppError?> = _appError

    private val _loading = MutableLiveData(false)
    val loading: LiveData<Boolean> = _loading

    fun updateAppError(error: AppError?) {
        _appError.value = error
    }

    fun clearError() {
        _appError.value = null
    }

    fun showLoading() {
        _loading.postValue(true)
    }

    fun hideLoading() {
        _loading.postValue(false)
    }

}
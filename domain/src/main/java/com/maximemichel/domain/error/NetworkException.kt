package com.maximemichel.domain.error

import java.io.IOException

class NetworkException(val code: HttpCode) : IOException()

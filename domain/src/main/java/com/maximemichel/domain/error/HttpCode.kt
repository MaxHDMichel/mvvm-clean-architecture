package com.maximemichel.domain.error

@Suppress("MagicNumber")
enum class HttpCode(val code: Int, val internalCode: Int) {
    OK(200, 1),
    BadRequest(400, 2),
    Unauthorized(401, 3),
    Forbidden(403, 4),
    NotFound(404, 5),
    RequestTimeout(408, 6),
    UnprocessableEntity(422, 7),
    InternalServerError(500, 8),
    ServiceUnavailable(503, 9),
    GatewayTimeout(504, 10),
    NoNetwork(-1, 11),
    Unknown(0, 0);

    companion object {
        fun from(code: Int) = values().find { it.code == code } ?: Unknown
    }
}

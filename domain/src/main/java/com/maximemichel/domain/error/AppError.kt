package com.maximemichel.domain.error

import javax.net.ssl.SSLPeerUnverifiedException

sealed class AppError {

    data class TextualError(
        val message: String? = null,
        val messageRes: Int? = null
    ) : AppError()

    data class CommonError(val code: CommonErrorCode) : AppError() {

        @Suppress("StringTemplate")
        fun getPrintableCode() = "$ERROR_CODE_PREFIX-${code.code}"

        companion object {
            const val ERROR_CODE_PREFIX = 1
        }
    }

    data class NetworkError(
        val httpCode: HttpCode
    ) : AppError() {

        @Suppress("StringTemplate")
        fun getPrintableCode() = "$ERROR_CODE_PREFIX-${httpCode.internalCode}"

        companion object {
            const val ERROR_CODE_PREFIX = 2
        }
    }
}

fun Exception.mapToAppError(): AppError {
    return when (this) {
        is NetworkException -> AppError.NetworkError(code)
        else -> mapToCommonError()
    }
}

fun Exception.mapToCommonError(): AppError {
    val code = when (this) {
        is StackOverflowError -> CommonErrorCode.StackOverflowError
        is NullPointerException -> CommonErrorCode.NullPointerException
        is OutOfMemoryError -> CommonErrorCode.OutOfMemoryError
        is IllegalStateException -> CommonErrorCode.IllegalStateException
        is IllegalArgumentException -> CommonErrorCode.IllegalArgumentException
        is ArrayIndexOutOfBoundsException -> CommonErrorCode.ArrayIndexOutOfBoundsException
        is SSLPeerUnverifiedException -> CommonErrorCode.SecurityCheck
        else -> CommonErrorCode.Unknown
    }

    return AppError.CommonError(code)
}

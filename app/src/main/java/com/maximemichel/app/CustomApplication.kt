package com.maximemichel.app

import androidx.multidex.MultiDexApplication
import com.maximemichel.app.conf.timberConf
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class CustomApplication : MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()
        timberConf()
    }
}